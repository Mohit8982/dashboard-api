const express = require('express');
const app = express();
const engine = require('ejs-locals');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
const methodOverride = require('method-override');
const responseTime = require('response-time');

//API ROUTES
const authRoute = require('./routes/API/auth');
const profileUpdate = require('./routes/API/profile/userProfile');
const mpin = require('./routes/API/mpin');
const funds = require('./routes/API/funds/fundRequest');
const htp = require('./routes/API/appSetting/htp');
const resulthistory = require('./routes/API/history/resultHistroy');
const bidsHistory = require('./routes/API/history/bidsHistory');
const Bids = require('./routes/API/bids/bids');
const newsNotification = require('./routes/API/common');

//INTERNAL ROUTES
const index = require('./routes/dashboard/index');
const user = require('./routes/dashboard/users');
const games = require('./routes/games/allGames');
const gameSetting = require('./routes/games/gameSetting');
const gameRates = require('./routes/games/gameList');
const gameResult = require('./routes/games/gameResult');
const winnerList = require('./routes/games/winnersList');
const starlineProvider = require('./routes/starline/starlineProvider');
const starlineSettings = require('./routes/starline/starGameSetting');
const starlineGameList = require('./routes/starline/starGameList');
const starlineResult = require('./routes/starline/starGameResult');
const andarBaharProvider = require('./routes/andarBahar/andarBaharProvider');
const andarBaharSettings = require('./routes/andarBahar/gameSetting');
const andarbahargamerates = require('./routes/andarBahar/gameList');
const andarbaharresult = require('./routes/andarBahar/gameResult');
const ABProfitLoss = require('./routes/andarBahar/andarBaharPF');
const fundData = require('./routes/wallet/fundReq');
const wallet = require('./routes/wallet/view_wallet');
const reqONOFF = require('./routes/wallet/reqON-OFF');
const approvedReports = require('./routes/dashboard/approvedReports');
const salesReport = require('./routes/reports/salesReport');
const totalBids = require('./routes/reports/totalBid');
const fundReport = require('./routes/reports/fundReport');
const creditDebit = require('./routes/reports/credit_debit');
const analysys = require('./routes/reports/marketRatioAnalysys');
const howPlay = require('./routes/appSetting/appSettings');
const master = require('./routes/master/bank');
const jodiAll = require('./routes/dashboard/jodiAll');
const common = require('./routes/dashboard/common');

//Connect To DB
dotenv.config();
mongoose.connect( process.env.DB_CONNECT , {useNewUrlParser: true}, ()=>{
    console.log('MONGO CONNECTED');
});

//Public
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'views')));

//body parser
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());
app.use(responseTime());
// view engine setup
app.engine('ejs', engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(methodOverride('_method'));

//Routing API middleware
app.use('/', index);
app.use('/api/user', authRoute);
app.use('/profile', profileUpdate);
app.use('/MPIN', mpin);
app.use('/fundreq', funds);
app.use('/history', resulthistory);
app.use('/bidsHistory', bidsHistory);
app.use('/Bids', Bids);
app.use('/nnData', newsNotification);

//Routing Dashboard middleware
app.use('/userList', user);
app.use('/games', games);
app.use('/masters', master);
app.use('/gamesSetting', gameSetting);
app.use('/gameList', gameRates);
app.use('/gameResult', gameResult);
app.use('/winner', winnerList);
app.use('/starlineProvider', starlineProvider);
app.use('/starlinegamesetting', starlineSettings);
app.use('/starlinegamerates', starlineGameList);
app.use('/starlinegameresult',starlineResult);
app.use('/providerAB', andarBaharProvider);
app.use('/andarbahargamesetting', andarBaharSettings);
app.use('/andarbahargamerates',andarbahargamerates);
app.use('/andarbahargameresult', andarbaharresult);
app.use('/ABProfitLoss', ABProfitLoss);
app.use('/fundRequest', fundData);
app.use('/customerBalance', wallet);
app.use('/reqOnOff',reqONOFF);
app.use('/approvedReports', approvedReports);
app.use('/salesReport', salesReport);
app.use('/totalBids', totalBids);
app.use('/fundReport', fundReport);
app.use('/creditDebit', creditDebit);
app.use('/analysys', analysys);
app.use('/appSettings', howPlay);
app.use('/htp', htp);
app.use('/common', common);
app.use('/jodiAll', jodiAll);

const port = process.env.port || 5000;
//Port
app.listen(port, () => {
    console.log(`Running on PORT ${port} `);
});
