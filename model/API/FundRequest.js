const mongoose = require('mongoose');

const fundreqSchema = new mongoose.Schema({
        userId:{
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        fullname:{
            type: String,
            required: true
        },
        username:{
            type: String,
            required: true
        },
        mobile:{
            type: String,
            required: true
        },
        reqAmount:{
            type: Number,
            required: true
        },
        reqType:{
            type: String,
            required: true
        },
        reqStatus:{
            type: String,
            required: true
        },
        reqDate:{
            type: String,
            required: true
        },
        reqTime:{
            type: String,
            required: true
        },
        withdrawalMode:{
            type: String,
            required: true
        },
        UpdatedBy:{
            type: String,
            required: true
        },
        adminId:{
            type: mongoose.Schema.Types.ObjectId,
            required: false
        },
        reqUpdatedAt:{
            type: String,
            required: true
        }
    },
    {
        versionKey : false
    });

module.exports = mongoose.model('fund_request', fundreqSchema);
