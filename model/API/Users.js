const mongoose = require('mongoose');
const  userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 3,
        max: 255
    },
    email: {
        type: String,
        required: true,
        max: 255,
        min: 6
    },
    password:{
        type: String,
        required: true,
        max: 1024,
        min: 6
    },
    username:{
        type: String,
        required: true,
        max: 1024,
        min: 6
    },
    verified:{
        type: String,
        required: true,
        max: 10,
        min: 6
    },
    role:{
        type: String,
        required: true,
        max: 10,
        min: 6
    },
    mobile:{
        type: String,
        required: true,
        max: 1024,
        min: 6
    },
    firebaseId:{
        type: String,
        required: true,
        max: 1024,
        min: 6
    },
    token:{
        type: String,
        required: true,
        max: 1024,
        min: 6
    },
    deviceName: {
        type: String,
        required: true,
        max: 255,
        min: 6
    },
    deviceId:{
        type: String,
        required: true,
        max: 1024,
        min: 6
    },
    banned:{
        type: String,
        required: true,
        max: 10,
        min: 6
    },
    wallet_balance:{
        type: Number,
        required: true
    },
    wallet_bal_updated_at:{
        type: String,
        required: true
    },
    mpin:{
        type: String,
        required: true
    },
    mpinOtp:{
        type: String,
        required: true
    },
    deviceVeriOTP:{
        type: String,
        required: true
    },
    CtreatedAt: {
        type: String,
        required: true
    },
    loginStatus: {
        type: String,
        required: true
        // 0 : Offline, 1 : Online
    }
},
{
  versionKey : false
});

module.exports = mongoose.model('Users', userSchema);

