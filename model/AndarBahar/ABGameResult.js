const mongoose = require('mongoose');
const abgamesResultSchema = new mongoose.Schema({

        providerId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        providerName: {
            type: String,
            required: true
        },
        session:{
            type: String,
            required: true
        },
        resultDate:{
            type: String,
            required: true
        },
        winningDigit:{
            type: Number,
            required: true
        },
        createdAt:{
            type: Date,
            default: Date.now,
            required: true
        },
        status:{
            type: String,
            required: true
        }
    },
    {
        versionKey : false
    });

module.exports = mongoose.model('ab_game_Result', abgamesResultSchema);
