const mongoose = require('mongoose');
const ABbidsSchema = new mongoose.Schema({
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        providerId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        gameTypeId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        userName: {
            type: String,
            required: true
        },    
        bidDigit:{
            type: String,
            required: true
        },
        biddingPoints:{
            type: Number,
            required: true
        },
        winStatus:{
            type: String,
            required: true
        },
        gameWinPoints:{
            type: Number,
            required: true
        },
        gameDate:{
            type: String,
            required: true
        },
        gameSession:{
            type: String,
            required: true
        },
        createdAt:{
            type: Date,
            default: Date.now
        },
        updatedAt:{
            type: String,
            required: true
        }
    },
    {
        versionKey : false
    });

module.exports = mongoose.model('ab_bids', ABbidsSchema);
