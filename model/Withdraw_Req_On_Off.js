const mongoose = require('mongoose');

const walletHistorySchema = new mongoose.Schema({
        date:{
            type: Date,
            default: Date.now
        },
        message:{
            type: String,
            required: true
        },
        enabled:{
            type: String,
            // 0 : Enabled, 1 : Disabled
            required: true
        },
        updatedAt:{
            type: String,
            required: true
        }
    },
    {
        versionKey : false
    });

module.exports = mongoose.model('wallet_history', walletHistorySchema);
