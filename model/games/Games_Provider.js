const mongoose = require('mongoose');
const gamesSchema = new mongoose.Schema({

        providerName: {
            type: String,
            required: true
        },
        providerResult:{
            type: String,
            required: true
        },
        modifiedAt:{
            type: String,
            required: true
        }

    },
    {
        versionKey : false
    });

module.exports = mongoose.model('games_provider', gamesSchema);
