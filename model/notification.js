const mongoose = require('mongoose');

const notificationSchema = new mongoose.Schema({
    title: {
            type: String,
            required: true
    },
    message: {
        type: String,
        required: true
    },
    modified:{
            type: Date,
            default: Date.now()
        }
    },
    {
        versionKey : false
    });

module.exports = mongoose.model('notification', notificationSchema);
