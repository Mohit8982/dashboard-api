const mongoose = require('mongoose');

const walletHistorySchema = new mongoose.Schema({
        userId:{
            type: mongoose.Schema.Types.ObjectId,
            required: true
        },
        previous_amount:{
            type: Number,
            required: true
        },
        current_amount:{
            type: Number,
            required: true
        },
        transaction_amount:{
            type: Number,
            required: true
        },
        description:{
            type: String,
            required: true
        },
        transaction_date:{
            type: String,
            required: true
        },
        transaction_status:{
            type: String,
            required: true
        },
        admin_id:{
            type: String,
            required: true
        },  
        addedBy_name:{
            type: String,
            required: true
        },
        particular:{
            type: String,
            required: true
        },
        reqType: {
            type: String,
            required: true
        },
        username: {
            type: String,
            required: true
        }
                
    },
    {
        versionKey : false
    });

module.exports = mongoose.model('wallet_history', walletHistorySchema);
