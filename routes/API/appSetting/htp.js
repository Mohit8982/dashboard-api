const router = require('express').Router();
const Rules = require('../../../model/appSetting/HowToPlay');
const NoticeBoard = require('../../../model/appSetting/NoticeBoard');
const ProfileNOte  = require('../../../model/appSetting/ProfileNote');
const WalletContact  = require('../../../model/appSetting/WalletContact');
const verify = require('../verifyTokens');

router.get('/', verify, async (req, res)=>{
    try {
        const data = await Rules.find();
        res.json({
                status: 1,
                message: "Success",
                data: data
            });
    }
    catch (e) {
        res.status(400).send(
            {
                status: 0,
                message: 'Something Happened Please Contact the Support',
                error: e
            });
    }
});

router.get('/noticeBoard', verify, async (req, res)=>{
    try {
        const data = await NoticeBoard.find();
        res.json({
            status: 1,
            message: "Success",
            data: data
        });
    }
    catch (e) {
        res.status(400).send({
            status: 0,
            message: 'Something Happened Please Contact the Support',
            error: e
        });
    }
});

router.get('/profileNote', verify, async (req, res)=>{
    try {
        const data = await ProfileNOte.find();
        res.json({
            status: 1,
            message: "Success",
            data: data
        });
    }
    catch (e) {
        res.status(400).send({
            status: 0,
            message: 'Something Happened Please Contact the Support',
            error: e
        });
    }
});

router.get('/walletContact', verify, async (req, res)=>{
    try {
        const data = await WalletContact.find();
        res.json({
            status: 1,
            message: "Success",
            data: data
        });
    }
    catch (e) {
        res.status(400).send({
            status: 0,
            message: 'Something Happened Please Contact the Support',
            error: e
        });
    }
});

module.exports = router;