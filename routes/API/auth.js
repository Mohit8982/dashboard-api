'use strict';
const router = require('express').Router();
const User = require('../../model/API/Users');
const jwt = require('jsonwebtoken');
const fundReq = require('../../model/API/FundRequest');
const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
const { registerValidation, loginValidation } = require('../../validation');
const dateTime = require('node-datetime');
const SendOtp = require('sendotp');
const sendOtp = new SendOtp('290393AuGCyi6j5d5bfd26');
const Pusher = require('pusher');
const channels_client = new Pusher({
    appId: '848468',
    key: '6e949bd79592c3ee46f7',
    secret: '015f8b25834d8d36ec1f',
    cluster: 'ap2'
});

dotenv.config();

router.post('/', async (req, res)=>{
    const mobileNumber = req.body.mobile;
    const user = await User.findOne({ mobile: req.body.mobile});
    if(user) res.status(400).send({response: 'User Already Registered With This Mobile Number', status: 0});
    sendOtp.send( mobileNumber, "KSGAME", function (error, data) {
        res.json({ status : 1, data : data});
    });
});

router.post('/register', async (req, res)=>{

    const mobileNumber = req.body.mobile;
    const OTP = req.body.deviceVeriOTP;
    sendOtp.verify( mobileNumber, OTP, async function (error, data) {
        if(data.type == 'success') {
            try {
                let data = req.header('x-api-key');
                if (!data) return res.status(400).send({
                    status: 0,
                    message: "Access Denied"
                });
                
                let buff = Buffer.from(data, 'base64');
                let text = buff.toString('ascii');
                const validAPI = await bcrypt.compare(process.env.REGISTER_API_KEY, text);
                if (!validAPI) return res.status(400).send({
                    status : 0,
                    message: "Access Denied"
                });
             
                //validate the data to be submitted
                const { error } = registerValidation(req.body);
                if (error) return  res.status(400).send(error.details[0].message);

                //check if USER already exist
                const emailExist = await User.findOne({ email: req.body.email, username: req.body.username,mobile: req.body.mobile,deviceId : req.body.deviceId });
                if (emailExist) return res.status(400).send({
                    status : 0,
                    message: "User Already Registered"
                });

                const salt =  await bcrypt.genSalt(10);
                const hashedPassword = await bcrypt.hash(req.body.password, salt);
                const dt = dateTime.create();
                const formatted = dt.format('Y-m-d H:M:S');
                const user = new User({
                    name: req.body.name,
                    email: req.body.email,
                    password: hashedPassword,
                    username: req.body.username,
                    verified: req.body.verified,
                    role: req.body.role,
                    mobile: req.body.mobile,
                    firebaseId: req.body.firebaseId,
                    token: req.body.token,
                    deviceName: req.body.deviceName,
                    deviceId: req.body.deviceId,
                    banned: req.body.banned,
                    wallet_balance: 0,
                    wallet_bal_updated_at: formatted,
                    deviceVeriOTP: req.body.deviceVeriOTP,
                    mpin: req.body.mpin,
                    mpinOtp: req.body.mpinOtp,
                    CtreatedAt: formatted,
                    loginStatus: 'offline'
                });

                try {
                    const savedUser = await user.save();
                    channels_client.trigger('my-channel', 'my-event', {
                        "message": user,
                        "toast" : "New User Registered"
                    });

                    const  token =  jwt.sign({key : user.deviceId }, process.env.jsonSecretToken);
                    const userData = {
                        token : token,
                        mobile : user.mobile,
                        username : user.username,
                        wallet_balance: user.wallet_balance,
                        userId : user._id
                    };

                    res.status(200).send({
                        status: 1,
                        message: "User Registered Successfully",
                        data : userData
                    });
                }
                catch (e) {
                    console.log(e);
                    res.status(400).send(e);
                }
            }
            catch (e) {
                res.json(e);
            }
        }
        if(data.type == 'error') res.json(data);
    });
});

//Login API
router.get('/login', async (req, res)=>{

    //validate the data to be submitted
    const { error } = loginValidation(req.body);
    if (error) return res.status(400).send(
        {
            status: 0,
            message:  error.details[0].message
        });

    //Checking if email exist or not
    const user = await User.findOne({username : req.body.username});
    if (!user) return res.status(400).send({
        status: 0,
        message: "Username Not Found"
    });

    //Password  Check
    const validPass = await bcrypt.compare(req.body.password, user.password);
    if (!validPass) return res.status(400).send({
        status: 0,
        message: "Invalid Uername or Password"
    });

    //assign and create token
    const  token =  jwt.sign({key : user.deviceId }, process.env.jsonSecretToken);
    const data = {
        token : token,
        mobile : user.mobile,
        username : user.username,
        wallet_balance: user.wallet_balance,
        userId : user._id
    };

    const  updateUser = await User.updateOne(
        {_id : user._id },
        { $set: { loginStatus: 'Online'} });

    res.header('auth-token', token).send({
        status: 1,
        message: "Success",
        data: data
    });
});


//MPIN LOGIN
router.get('/mpinLogin', async (req, res)=> {
    try {
        const user = await User.findOne({ _id: req.body.id});
        if (user == null) res.status(400).send({
            status: 0,
            message: 'MPIN Not Available or Invalid'
        });

        const validPass = await bcrypt.compare(req.body.mpin, user.mpin);
        if(!validPass)
        {
            res.status(400).send({
                status: 0,
                message: 'Invalid MPIN'
            });
        }
        else {
        const  token =  jwt.sign({key : user.deviceId }, process.env.jsonSecretToken);
        const data = {
            token : token,
            mobile : user.mobile,
            username : user.username,
            wallet_balance: user.wallet_balance,
            userId : user._id
        };
        const  updateUser = await User.updateOne(
            {_id : user._id },
            { $set: { loginStatus: 'Online'} });

            res.header('auth-token', token).send({
                status: 1,
                message: "Success",
                data: data
            });
        }
    }
    catch (e) {
        console.log(e);
        res.json(e);
    }
});

//logout
router.get('/logout', async (req, res)=> {
    try {
        const  updateUser = await User.updateOne(
            {_id : req.body.userId },
            { $set: { loginStatus: 'offline'} });
        res.json({
            status: 1,
            message: "Logged Out Successfully"
        });
        }
    catch (e) {
        console.log(e);
        res.json(e);
    }
});

module.exports = router;