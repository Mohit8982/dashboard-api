'use strict';
const router = require('express').Router();
const jwt = require('jsonwebtoken');
const User = require('../../model/API/Users');
const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
const { registerValidation, loginValidation } = require('../../validation');
var dateTime = require('node-datetime');
dotenv.config();

router.post('/register', async (req, res)=>{

    let data = req.header('x-api-key');

    if(!data) return res.status(400).send("Access Denied");
    let buff = Buffer.from(data, 'base64');
    let text = buff.toString('ascii');

    const validAPI = await bcrypt.compare(process.env.REGISTER_API_KEY, text);
    if (!validAPI) return res.status(400).send("Access Denied");
    //validate the data to be submitted
    const { error } = registerValidation(req.body);
    if (error) return  res.status(400).send(error.details[0].message);

    //check if Email already exist
    const emailExist = await User.findOne({ email: req.body.email });
    if (emailExist) return res.status(400).send('Email Already Exist');
    //check if Username already exist
    const usernameExist = await User.findOne({ username: req.body.username });
    if (usernameExist) return res.status(400).send('Username Already Exist');
    //check if Mobile Number already exist
    const mobileExist = await User.findOne({ mobile: req.body.mobile });
    if (mobileExist) return res.status(400).send('Mobile Number Already Register');
    //check if Device already exist
    const deivceExist = await User.findOne({ deviceId : req.body.deviceId });
    if (deivceExist) return res.status(400).send('Account Already Registered With This Device');

    const salt =  await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    //banned status : 0 => User Is Blocked and vice versa

    const dt = dateTime.create();
    const formatted = dt.format('Y-m-d H:M:S');

    const user = new User({
        userId: req.body.userId,
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword,
        username: req.body.username,
        verified: req.body.verified,
        role: req.body.role,
        mobile: req.body.mobile,
        firebaseId: req.body.firebaseId,
        token: req.body.token,
        deviceName: req.body.deviceName,
        deviceId: req.body.deviceId,
        banned: req.body.banned,
        smsSent: req.body.smsSent,
        mpin: req.body.mpin,
        mpinOtp: req.body.mpinOtp,
        CtreatedAt: formatted
    });

    try {
        const savedUser = await user.save();
        res.send({ user: user._id });
    }
    catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;
