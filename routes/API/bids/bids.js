const router = require('express').Router();
const verify = require('../verifyTokens');
const bids = require('../../../model/AndarBahar/ABbids');
const Sbids = require('../../../model/starline/StarlineBids');
const Gbigs = require('../../../model/games/gameBids');


router.get('/', verify, async(req, res)=>{
    res.json(
        {   status: 0,
            message: "Access Denied"
        });
});

router.post('/abBids', verify ,async (req, res)=>{

    const bidDatarray  = req.body;
    try {
        const arr = await bids.insertMany(bidDatarray);
        res.status(200).send(
            {
                status: 1,
                message: 'Inserted Successfully'
            });
    }
    catch (e) {
        console.log(e);
        res.json({
            status: 0,
            message: "Something Bad Happened Please Contact Support",
            error : e
        });
    }
});

router.post('/starLineBids', verify ,async (req, res)=>{
    const bidDatarray  = req.body;
    try {
        const arr = await Sbids.insertMany(bidDatarray);
        res.status(200).send({
            status: 1,
            message: 'Inserted Successfully'
        });
    }
    catch (e) {
        console.log(e);
        res.json({
            status: 0,
            message: "Something Bad Happened Contact The Support",
            error : e
        });
    }
});

router.post('/game_bids', verify ,async (req, res)=>{
    const bidDatarray  = req.body;
    try {
        const arr = await Gbigs.insertMany(bidDatarray);
        res.status(200).send({
            status: 1,
            message: 'Inserted Successfully'
        });
    }
    catch (e) {
        console.log(e);
        res.json({
            status: 0,
            message: "Something Bad Happened Contact The Support",
            error : e
        });    }
});
module.exports = router;