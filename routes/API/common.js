const router = require('express').Router();
const news = require('../../model/News');
const notification = require('../../model/notification');
const verify = require('./verifyTokens');

router.get('/news',verify, async (req, res)=>{
    try {
        const NewsData = await news.find();        
        res.json({
            status: 1,
            message: "Success",
            data: NewsData
        });
    } catch (error) {
        res.json({
            status: 0,
            message: "Something Bad Happened Please Contact Support",
            error : error
        });
    }
});

router.get('/notification',verify, async (req, res)=>
{
    try {
        const notificationsData = await notification.find().sort({_id: -1});
        res.json({
            status: 1,
            message: "Success",
            data: notificationsData
        })
    }
    catch (e) {
        res.json({
            status: 0,
            message: "Something Bad Happened Please Contact Support",
            error : e
        });
    }
});

module.exports= router;