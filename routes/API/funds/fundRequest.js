const router = require('express').Router();
const fundReq = require('../../../model/API/FundRequest');
const User = require('../../../model/API/Users');
const verify = require('../verifyTokens');
const Pusher = require('pusher');

router.get('/', (req, res)=>{
    res.json(
        {   status: 0,
            message: "Access Denied"
        });
});

router.post('/addFund', verify, async (req, res)=>{
    const channels_client = new Pusher({
        appId: '848468',
        key: '6e949bd79592c3ee46f7',
        secret: '015f8b25834d8d36ec1f',
        cluster: 'ap2'
    });
    const addReq = new fundReq({
        userId : req.body.user_id,
        reqAmount : req.body.req_amount,
        fullname : req.body.fullname,
        username : req.body.username,
        mobile  : req.body.mobile,
        reqType : 'Credit',
        reqStatus :  'Pending',
        reqDate : req.body.req_date,
        reqTime : req.body.req_time,
        withdrawalMode: 'null',
        UpdatedBy: 'null',
        reqUpdatedAt: 'null'
    });
    try
    {
        const user = await User.findOne({ _id: req.body.user_id});
        if(!user)
        {
            res.json({
                status: 0,
                message: "User Not Found"
            });
        }
        else {
            const save = await addReq.save();
            channels_client.trigger('my-channel', 'my-event', {
                "message": addReq,
                "toast" : "New Credit Request Raised, Review The Changes.",
                "type" : 1
            });
            res.status(200).send({
                    status: 1,
                    message: 'Add Fund Request Raised Successfully'
                });
        }
    }
    catch (e) {
        console.log(e);
        res.json({
            status: 0,
            message: "Something Bad Happened Please Contact Support",
            error : e
        });
    }
});

router.post('/withdrawFund', verify, async(req, res)=>{

    const channels_client = new Pusher({
        appId: '848468',
        key: '6e949bd79592c3ee46f7',
        secret: '015f8b25834d8d36ec1f',
        cluster: 'ap2'
    });
    const addReq = new fundReq({
        userId : req.body.user_id,
        reqAmount : req.body.req_amount,
        fullname : req.body.fullname,
        username : req.body.username,
        mobile  : req.body.mobile,
        reqType : 'Debit',
        reqStatus :  'Pending',
        reqDate : req.body.req_date,
        reqTime : req.body.req_time,
        withdrawalMode: req.body.withdrawalMode,
        UpdatedBy: 'null',
        reqUpdatedAt: 'null'
    });
    try
    {
        const user = await User.findOne({ _id: req.body.user_id});
        if(!user)
        {
            res.status(400).send({
                status: 0,
                message: "NO User Exist With Name :" + req.body.username
            });
        }
        else {
            const save = await addReq.save();
            channels_client.trigger('my-channel', 'my-event', {
                "message": addReq,
                "toast" : "New Withdraw Request Raised, Review The Changes."
            });
            res.json({
                status: 1,
                message: 'Withdraw Request Raised Successfully'
            });
        }
    }
    catch (e) {
        res.json({
            status: 0,
            message: "Something Bad Happened Please Contact Support",
            error : e
        });
    }
});

module.exports = router;