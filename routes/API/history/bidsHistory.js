const router = require('express').Router();
const starline = require('../../../model/starline/StarlineBids');
const AB = require('../../../model/AndarBahar/ABbids');
const game_bids = require('../../../model/games/gameBids');
const verify = require('../verifyTokens');

router.get('/stalineBids', verify, async(req, res)=>{
    try {
        const bidData = await starline.find({userId : req.body.userId});
        res.status(200).send({
            status: 1,
            message: "Success",
            data: bidData
        });
    }
    catch (e) {
        console.log(e);
        res.json({
            status: 0,
            message: "Something Bad Happened Please Contact Support",
            error : e
        });
    }
});

router.get('/abBids', verify, async(req, res)=>{
    try {
        const bidData = await AB.find({userId : req.body.userId});
        res.status(200).send({
            status: 1,
            message: "Success",
            data: bidData
        });
    }
    catch (e) {
       
        res.json({
            status: 0,
            message: "Something Bad Happened Please Contact Support",
            error : e
        });
    }
});

router.get('/gameBids', verify, async(req, res)=>{
    try {
        const bidData = await game_bids.find({userId : req.body.userId});
        res.status(200).send({
            status: 1,
            message: "success",
            data : bidData
        });
    }
    catch (e) {
      
        res.json({
            status: 0,
            message: "Something Bad Happened Please Contact Support",
            error : e
        });
    }
});

module.exports = router;
