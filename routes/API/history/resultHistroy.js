const router = require('express').Router();
const starline = require('../../../model/starline/GameResult');
const AB = require('../../../model/AndarBahar/ABGameResult');
const fund = require('../../../model/API/FundRequest');
const verify = require('../verifyTokens');

router.get('/starlineHistory', verify, async (req, res)=>{
    const date = req.body.date;
    try {
        const resultData = await starline.find({resultDate : date});
        if (Object.keys(resultData).length === 0){
            res.json({
                status: 0,
                message: "No Result Found For Date : " + date
            });
        }
        else{
            res.json({
                status: 1,
                message: "Success",
                data: resultData
            });
        }
    }
    catch (e) {
        res.json({
            status: 0,
            message: "Something Bad Happened Please Contact Support",
            error : e
        });
    }
});

router.get('/fundRequestHistory', verify, async (req, res)=>{
    try {
        const resultData = await fund.find({userId : req.body.id});
        if (Object.keys(resultData).length === 0){
            res.json({
                status: 0,
                message: "No Fund Request History"
            });
        }
        else{
            res.json({
                status: 1,
                message: "Success",
                data: resultData
            });
        }
    }
    catch (e) {
        res.json({
            status: 0,
            message: "Something Bad Happened Please Contact Support",
            error : e
        });
    }
});

router.get('/creditRequestHistory', verify, async (req, res)=>{
    try {
        const resultData = await fund.find({userId : req.body.id, reqStatus: "Approved" , reqType: "Credit"});
        if (Object.keys(resultData).length === 0){
            res.json({
                status: 0,
                message: "No Credit Request History"
            });
        }
        else{
            res.json({
                status: 1,
                message: "Success",
                data: resultData
            });
        }
    }
    catch (e) {
        res.json({
            status: 0,
            message: "Something Bad Happened Please Contact Support",
            error : e
        });
    }
});

router.get('/debitRequestHistory', verify, async (req, res)=>{
    try {
        const resultData = await fund.find({userId : req.body.id, reqStatus: "Approved" , reqType: "Debit"});
        if (Object.keys(resultData).length === 0){
            res.json({
                status: 0,
                message: "No Debit Request History"
            });
        }
        else{
            res.json({
                status: 1,
                message: "Success",
                data: resultData
            });
        }
    }
    catch (e) {
        res.json({
            status: 0,
            message: "Something Bad Happened Please Contact Support",
            error : e
        });
    }
});

router.get('/andarBaharRequestHistory', verify, async (req, res)=>{
    const date = req.body.date;
    try {
        const resultData = await AB.find({resultDate : date});
        if (Object.keys(resultData).length === 0){
            res.json({
                status: 0,
                message: "No Result Found For Date : " + date
            });
        }
        else{
            res.json({
                status: 1,
                message: "Success",
                data: resultData
            });
        }
    }
    catch (e) {
        res.json({
            status: 0,
            message: "Something Bad Happened Please Contact Support",
            error : e
        });
    }
});

module.exports = router;
