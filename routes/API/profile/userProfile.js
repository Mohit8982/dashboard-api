const router = require('express').Router();
const profile = require('../../../model/API/Profile');
const verify = require('../verifyTokens');
const dateTime = require('node-datetime');
const dt = dateTime.create();
const formatted = dt.format('Y-m-d H:M:S');

router.post('/addAddress', verify ,async (req, res)=>{

	try {
		const addressData = new profile({
			userId: req.body.userId,address: req.body.address,city: req.body.city,pincode: req.body.pincode,created_at: formatted
		});
		const user = await profile.findOne({userId : req.body.userId});
		if(!user){
			const savedUser = await addressData.save();
			res.send({
				status: 1,
				message: "Success"
			});
		}
		else {
			const updateUser = await profile.updateOne(
				{ userId: req.body.userId },
				{ $set: { userId: req.body.userId,address: req.body.address,city: req.body.city,pincode: req.body.pincode,created_at: formatted,updatedAt: formatted}});
			res.send({
				status: 1,
				response: "Updated Successfully",
			});
		}
		
	} catch (error) {
		res.status(400).send({
			status: 0,
			message: "Something Happened Please Try Again After Some Time",
			error: error, 
		})
	}
});

router.post('/bankdetails', verify, async (req, res)=>{
	try {
		const bankDetails = new profile({
			userId: req.body.userId, account_no: req.body.account_no, bank_name: req.body.bank_name, ifsc_code: req.body.ifsc_code,	account_holder_name: req.body.account_holder_name, created_at: formatted
		});
		
		const user = await profile.findOne({userId : req.body.userId});
		if(!user){
			const savedUser = await bankDetails.save();
			res.send({
				status : 1,
				message: "success",
				data : savedUser
			});
		}
		else {
			const updateDetails = await profile.updateOne(
				{ userId: req.body.userId },
				{ $set: { account_no: req.body.account_no, bank_name: req.body.bank_name, ifsc_code: req.body.ifsc_code, account_holder_name: req.body.account_holder_name, updatedAt: formatted }});
			res.send({status: 1,message : "Updated Successfully"});
		}
	} catch (error) {
		res.status(400).send({
			status: 0,
			message: "Something Went Worng",
			error : error
		})
	}
});

router.post('/phoneNumber', verify, async (req, res)=>{
	try {
		const bankDetails = new profile({
			userId: req.body.userId, paytm_number: req.body.paytm_number, phonePe_number: req.body.phonePe_number, tez_number: req.body.tez_number, created_at: formatted
		});
		
		const user = await profile.findOne({userId : req.body.userId});
		if(!user){
			const savedUser = await bankDetails.save();
			res.send({
				status : 1,
				message: "success",
				data : savedUser
			});
		}
		else {
			const updateNumbers = await profile.updateOne(
				{ userId: req.body.userId },
				{ $set: { paytm_number: req.body.paytm_number, phonePe_number: req.body.phonePe_number, tez_number: req.body.tez_number, created_at: formatted }});
				res.send({status: 1,message : "Updated Successfully"});
		}
	} catch (error) {
		res.status(400).send({
			status: 0,
			message: "Something Went Worng",
			error : error
		})
	}
	
});

module.exports = router;