const jwt = require('jsonwebtoken');

module.exports = function ( req,res,next) {

    const token = req.header('auth-token');
	if (!token){
        return res.status(401).json({
            status: 0,
            message: 'Access Denied'
        });
	}
    try {
        const verified = jwt.verify(token, process.env.jsonSecretToken);
        req.user = verified;
        next();
    }
    catch (e) {
        res.status(400).json({
            status: 0,
            message: 'Invalid Token'
        });
    }
};
