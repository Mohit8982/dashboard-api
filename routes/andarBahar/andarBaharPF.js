const router = require('express').Router();
const ABList = require('../../model/AndarBahar/ABProvider');
const ABtype = require('../../model/AndarBahar/ABGameList');
const ABbids = require('../../model/AndarBahar/ABbids');
const mongodb = require('mongodb');

router.get('/', async (req, res)=>{
    try {
        const provider = await ABList.find({}, { providerName: 1, _id: 1 });
        res.render('./andarbahar/ABprofitloss', { data: provider });
    } catch (e) {
        res.json({ message: e });
    }
});

router.post('/getResult', async (req, res) => {
    const provider = req.body.provider;
    const date = req.body.date;
    try
    {  
        const type = await ABtype.find({}, { gamePrice: 1, _id: 1, gameName: 1, _id: 1 });
        const data1 = await ABbids.aggregate([
            {
                $match: {
                    providerId: mongodb.ObjectId(provider),
                    gameDate: date
                }
            },
            { 
                $group: { 
                    _id: '$gameTypeId',
                    sumdigit: { $sum: '$biddingPoints' },
                    countBid: { $sum: 1 },
                    "gameType": { "$first" : "$gameSession"}
                }
            }
        ]);

        const data2 = await ABbids.aggregate([
            {
                $match: {
                    providerId: mongodb.ObjectId(provider),
                    gameDate: date
                }
            },
            { 
                $group: { 
                    _id: {bidDigit : '$bidDigit',providerId:'$providerId', gameTypeId:'$gameTypeId'},
                    sumdigit: { $sum: '$biddingPoints' },
                    countBid: { $sum : 1 }
                }
            }
        ]);

        res.json({ data: data1, data2: data2, type : type });
    } catch (error) {
        console.log(error);
    }
});

module.exports = router;