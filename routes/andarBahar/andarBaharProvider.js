const router = require('express').Router();
const ABProvider = require('../../model/AndarBahar/ABProvider');
const dateTime = require('node-datetime');

router.get('/', async (req, res)=>{
    try {
        const provider = await ABProvider.find();
        res.render('./andarbahar/ABprovider', { data: provider });
    } catch (e) {
        res.json({ message: e });
    }
});

router.get('/specificUser', async (req, res)=>{
    try {
        const user = await ABProvider.findOne({ _id: req.query.userId});
        res.json(user);
    } catch (e) {
        res.json({ message: e });
    }
});

router.post('/insertGame', async (req, res)=>{
    const dt = dateTime.create();
    const formatted = dt.format('Y-m-d H:M:S');
    const games = new ABProvider({
        providerName:  req.body.gamename,
        providerResult: req.body.result,
        modifiedAt: formatted
    });
    try {
        const savedGames = await games.save();
        res.redirect('/providerAB');
    }
    catch (e) {
        res.status(400).send(e);
    }
});

router.delete('/', async (req, res)=>{
    try {
        const savedGames = await ABProvider.deleteOne({ _id: req.body.userId});
        res.json(savedGames);
    }
    catch (e) {
        res.json(e);
    }
});

router.patch('/', async (req, res)=>{
    try {
        const dt = dateTime.create();
        const formatted = dt.format('Y-m-d H:M:S');
        const  updateUser = await  ABProvider.updateOne(
            { _id: req.body.userId },
            { $set: { providerName : req.body.gamename, providerResult: req.body.result, modifiedAt: formatted } });
        res.redirect('/providerAB');
    }
    catch (e) {
        res.json(e);
    }
});
module.exports = router;
