const router = require('express').Router();
const ABgameList = require('../../model/AndarBahar/ABGameList');
const dateTime = require('node-datetime');

router.get('/', async (req, res)=>{
    try {
        const provider = await ABgameList.find();
        res.render('./andarbahar/ABgamerates', { data: provider });
    } catch (e) {
        res.json({ message: e });
    }
});

router.post('/insertGame', async (req, res)=>{

    const dt = dateTime.create();
    const formatted = dt.format('Y-m-d H:M:S');
    const games = new ABgameList({
        gameName:  req.body.gameName,
        gamePrice: req.body.gamePrice,
        modifiedAt: formatted
    });
    try {
        const savedGames = await games.save();
        const provider = await ABgameList.find();
        res.status(200).send(provider);
    }
    catch (e) {
        console.log(e);
        res.status(400).send(e);
    }
});

router.delete('/', async (req, res)=>{
    try {
        const savedGames = await ABgameList.deleteOne({ _id: req.body.userId});
        res.json(savedGames);
    }
    catch (e) {
        res.json(e);
    }
});

router.post('/update', async (req, res)=>{
    try {

        const dt = dateTime.create();
        const formatted = dt.format('Y-m-d H:M:S');

        const  updateUser = await  ABgameList.updateOne(
            { _id: req.body.userId },
            { $set: { gameName : req.body.gameName, gamePrice : req.body.gamePrice, modifiedAt: formatted } });

        const provider = await ABgameList.find();
        res.status(200).send(provider);
    }
    catch (e) {
        res.json(e);
    }
});

module.exports = router;
