const router = require('express').Router();
const ABgamesProvider = require('../../model/AndarBahar/ABProvider');
const ABgamesSetting = require('../../model/AndarBahar/ABAddSetting');
const dateTime = require('node-datetime');
const mongoose = require('mongoose');

router.get('/', async (req, res)=>{
    try {
        const id = req.query.userId;
        ABgamesProvider.aggregate([
            {
                $lookup: {
                    from: 'ab_games_settings',
                    localField: '_id',
                    foreignField: 'providerId',
                    as: 'gameDetails'
                }
            }
            ],function (error, lookup) {
            if (error) throw error;
            else  {
              if (id == 123456)
              { res.json(lookup); }
              else {res.render('./andarbahar/ABgamesetting', { data: lookup });}
            }
        });
    } catch (e) {
        res.json({ message: e });
    }
});

router.get('/addSetting', async (req, res)=>{
    try {
        const provider = await ABgamesProvider.find();
        res.render('./andarbahar/ABaddsetting', { data: provider });
    } catch (e) {
        res.json({ message: e });
    }
});

router.post('/insertSettings', async (req, res)=>{
    const dt = dateTime.create();
    const formatted = dt.format('Y-m-d H:M:S');
    const settings = new ABgamesSetting({
        providerId : req.body.gameid,
        gameDay: req.body.gameDay,
        OBT: req.body.game1,
        CBT: req.body.game2,
        OBRT: req.body.game3,
        CBRT: req.body.game4,
        isClosed: req.body.status,
        modifiedAt: formatted
    });
    try {
        const savedSettings = await settings.save();
        res.redirect('/andarbahargamesetting');
    }
    catch (e) {
        res.status(400).send(e);
    }
});


router.patch('/', async (req, res)=>{
    try {
        const dt = dateTime.create();
        const formatted = dt.format('Y-m-d H:M:S');
        const  updateUser = await  ABgamesSetting.updateOne(
            { _id: req.body.id },
            { $set: { OBT : req.body.obt, CBT: req.body.cbt, OBRT:  req.body.obrt, CBRT : req.body.cbrt, isClosed: req.body.close, modifiedAt:  formatted, } });
        res.redirect('/andarbahargamesetting');
    }
    catch (e) {
        res.json(e);
    }
});

module.exports = router;
