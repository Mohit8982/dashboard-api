const router = require('express').Router();
const Rules = require('../../model/appSetting/HowToPlay');
const Notice = require('../../model/appSetting/NoticeBoard');
const ProfileNote = require('../../model/appSetting/ProfileNote');
const WalletContact = require('../../model/appSetting/WalletContact');
const dateTime = require('node-datetime');
const dt = dateTime.create();
const formatted = dt.format('Y-m-d H:M:S');

router.get('/HTP', async (req, res)=>{
    const find = await Rules.find();
    if (Object.keys(find).length === 0)
    {
        const addData = new Rules({
            title: 'Welcome To Kuber StarLine',
            Description: 'Coming Soon',
            videoUrl: 'Coming Soon'
        });
        const save = await addData.save();
        let array = [save];
        res.render('./appSettings/howToPlay', {data : array});
    }
    else {
        res.render('./appSettings/howToPlay', {data : find});
    }
});

router.post('/update', async (req, res)=>
{
   try {
        const  updateUser = await Rules.updateOne(
            {_id : req.body.id },
            { $set: { title: req.body.title, Description: req.body.desc, videoUrl: req.body.videoUrl} });
        const user = await Rules.findOne({ _id: req.body.id});
        res.status(200).json(user);
    }
    catch (e) {
        console.log(e);
        res.json(e);
    }
});

router.get('/noticeBoard', async (req, res)=>{
    const find = await Notice.find();
    if (Object.keys(find).length === 0)
    {
        const addData = new Notice({
            title1: 'Welcome To Kuber StarLine',
            title2: 'Welcome To Kuber StarLine',
            title3: 'Welcome To Kuber StarLine',
            description1: 'Coming Soon',
            description2: 'Coming Soon',
            description3: 'Coming Soon',
            contact: 'Coming Soon'
        });
        const save = await addData.save();
        let array = [save];
        res.render('./appSettings/noticeBoard', {data : array});
    }
    else {
        res.render('./appSettings/noticeBoard', {data : find});
    }

});

router.post('/updateNotice', async (req, res)=>
{
    try {
        const  updateUser = await Notice.updateOne(
            {_id : req.body.id },
            { $set: { title1: req.body.title1,title2: req.body.title2,title3: req.body.title3, description1: req.body.desc1,description2: req.body.desc2,description3: req.body.desc3,contact : req.body.contact} });
        const user = await Notice.findOne({ _id: req.body.id});
        res.status(200).json(user);
    }
    catch (e) {
        console.log(e);
        res.json(e);
    }
});

router.get('/profileNote', async (req, res)=>{

    const find = await ProfileNote.find();
    if (Object.keys(find).length === 0)
    {
        const addData = new ProfileNote({
            note: 'Welcome To Kuber StarLine'
        });
        const save = await addData.save();
        let array = [save];
        res.render('./appSettings/profileNote', {data : array});
    }
    else {
        res.render('./appSettings/profileNote', {data : find});
    }

});

router.post('/updateProfileNote', async (req, res)=>
{
    try {
        const  updateUser = await ProfileNote.updateOne(
            {_id : req.body.id },
            { $set: { note: req.body.note} });
        const user = await ProfileNote.findOne({ _id: req.body.id});
        res.status(200).json(user);
    }
    catch (e) {
        console.log(e);
        res.json(e);
    }
});

router.get('/walletContact', async (req, res)=>{

    const find = await WalletContact.find();
    if (Object.keys(find).length === 0)
    {
        const addData = new WalletContact({
            number: '**********'
        });
        const save = await addData.save();
        let array = [save];
        res.render('./appSettings/walletContact', {data : array});
    }
    else {
        console.log(find);
        res.render('./appSettings/walletContact', {data : find});
    }

});

router.post('/updatewalletContact', async (req, res)=>
{
    try {
        const  updateUser = await WalletContact.updateOne(
            {_id : req.body.id },
            { $set: { number: req.body.number} });
        const user = await WalletContact.findOne({ _id: req.body.id});
        res.status(200).json(user);
    }
    catch (e) {
        console.log(e);
        res.json(e);
    }
});
module.exports = router;
