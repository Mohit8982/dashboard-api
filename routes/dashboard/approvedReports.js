const router = require('express').Router();
const fundReq = require('../../model/API/FundRequest');
const dateTime = require('node-datetime');
const dt = dateTime.create();
let date = dt.format('m/d/Y');

router.get('/bank', async (req, res)=>{
    try {
        const report = await fundReq.find({reqDate : date, reqStatus: "Approved", withdrawalMode: "Bank"});
        fundReq.aggregate([
            {
                $match : {
                    $and:[{  reqDate : date,reqStatus: "Approved",withdrawalMode: "Bank" }]
                }
            },
            {
                $group : {
                    _id : '',
                    total : { $sum : "$reqAmount" }
                }
            }
        ], function (err, result) {
            if (err){throw  err}
            else {
                if (Object.keys(result).length === 0)
                {
                   res.render('approvedReports/bankAccount', {data : report, total: 0});
                }
                else {
                   res.render('approvedReports/bankAccount', {data : report, total: result[0].total});
                }
            }
        });
    }catch (e) {
        console.log(e);
        res.json({message : e});
    }
});


router.get('/paytm', async (req, res)=>{
    try {

        const report = await fundReq.find({reqDate : date, reqStatus: "Approved", withdrawalMode: "paytm"});
        fundReq.aggregate([
            {
                $match : {
                    $and:[{  reqDate : date,reqStatus: "Approved",withdrawalMode: "paytm" }]
                }
            },
            {
                $group : {
                    _id : '',
                    total : { $sum : "$reqAmount" }
                }
            }
        ], function (err, result) {
            if (err){throw  err}
            else {
                if (Object.keys(result).length === 0)
                {
                    res.render('approvedReports/paytm', {data : report, total: 0});
                }
                else {
                    res.render('approvedReports/paytm', {data : report, total: result[0].total});
                }
            }
        });
    }catch (e) {
        res.json({message : e});
    }
});

router.get('/paytm_ajax', async (req, res)=>{

    const date_cust = req.query.date_cust;
    try {
        const report = await fundReq.find({reqDate : date_cust, reqStatus: "Approved", withdrawalMode: "paytm"});
        fundReq.aggregate([
            {
                $match : {
                    $and:[{  reqDate : date_cust,reqStatus: "Approved",withdrawalMode: "paytm" }]
                }
            },
            {
                $group : {
                    _id : '',
                    total : { $sum : "$reqAmount" }
                }
            }
        ], function (err, result) {
            if (err){throw  err}
            else {
                if (Object.keys(result).length === 0){
                    const bind = ({
                        approvedData : report,
                        total  : 0
                    });
                    res.json(bind);
                }
                else{
                    const bind = ({
                        approvedData : report,
                        total  : result[0].total
                    });
                    res.json(bind);
                }
            }
        });
    }catch (e) {
        res.json({message : e});
    }
});

router.get('/bank_ajax', async (req, res)=>{

    const date_cust = req.query.date_cust;
    try {
        const report = await fundReq.find({reqDate : date_cust, reqStatus: "Approved", withdrawalMode: "Bank"});
        fundReq.aggregate([
            {
                $match : {
                    $and:[{  reqDate : date_cust,reqStatus: "Approved",withdrawalMode: "Bank" }]
                }
            },
            {
                $group : {
                    _id : '',
                    total : { $sum : "$reqAmount" }
                }
            }
        ], function (err, result) {
            if (err){throw  err}
            else {
                if (Object.keys(result).length === 0){
                    const bind = ({
                        approvedData : report,
                        total  : 0
                    });
                    res.json(bind);
                }
                else{
                    const bind = ({
                        approvedData : report,
                        total  : result[0].total
                    });
                    res.json(bind);
                }
            }
        });
    }catch (e) {
        res.json({message : e});
    }
});


//Declined Reports Route

router.get('/declined', async (req, res)=>{
    try {

        const report = await fundReq.find({reqDate : date, reqStatus: "Approved"});
        fundReq.aggregate([
            {
                $match : {
                    $and:[{  reqDate : date,reqStatus: "Approved"}]
                }
            },
            {
                $group : {
                    _id : '',
                    total : { $sum : "$reqAmount" }
                }
            }
        ], function (err, result) {
            if (err){throw  err}
            else {
                if (Object.keys(result).length === 0)
                {
                    res.render('dashboard/declinedReports', {data : report, total: 0});
                }
                else {
                    res.render('dashboard/declinedReports', {data : report, total: result[0].total});
                }
            }
        });
    }catch (e) {
        res.json({message : e});
    }
});

router.get('/declined_ajax', async (req, res)=>{

    const date_cust = req.query.date_cust;
    try {
        const report = await fundReq.find({reqDate : date_cust, reqStatus: "Declined"});
        fundReq.aggregate([
            {
                $match : {
                    $and:[{  reqDate : date_cust,reqStatus: "Declined"}]
                }
            },
            {
                $group : {
                    _id : '',
                    total : { $sum : "$reqAmount" }
                }
            }
        ], function (err, result) {
            if (err){throw  err}
            else {
                if (Object.keys(result).length === 0){
                    const bind = ({
                        approvedData : report,
                        total  : 0
                    });
                    res.json(bind);
                }
                else{
                    const bind = ({
                        approvedData : report,
                        total  : result[0].total
                    });
                    res.json(bind);
                }
            }
        });
    }catch (e) {
        res.json({message : e});
    }
});

module.exports = router;
