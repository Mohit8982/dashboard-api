const router = require('express').Router();
const news = require('../../model/News');
const notification = require('../../model/notification');
const dateTime = require('node-datetime');
const dt = dateTime.create();
const formatted = dt.format('Y-m-d H:M:S');

router.get('/news', async (req, res)=>{
    const find = await news.find();
    if (Object.keys(find).length === 0)
    {
        const addData = new news({
            Description: 'News Coming Soon, Stay Tuned...',
            modified: formatted
        });
        const save = await addData.save();
        let array = [save];
        res.render('./dashboard/news', {data : array});
    }
    else {
        res.render('./dashboard/news', {data : find});
    }
});

router.post('/news', async (req, res)=>
{
    try {
        const  newsData = await news.updateOne(
            {_id : req.body.id },
            { $set: { Description: req.body.note, modified: formatted} });
        const newsNew = await news.findOne({ _id: req.body.id});
        res.status(200).json(newsNew);
    }
    catch (e) {
        console.log(e);
        res.json(e);
    }
});

router.get('/notification', async (req, res)=>
{
    try {
        const find = await notification.find().sort({_id: -1});
        res.render('./dashboard/notification', {data : find});
    }
    catch (e) {
        console.log(e);
        res.json(e);
    }
});


router.post('/notification', async (req, res)=>
{
    try {
        const noti = new notification({
            title: req.body.title,
            message : req.body.message
        })
        const send = await noti.save();
        const notiData = await notification.find().sort({_id: -1});
        res.json(notiData);
    }
    catch (e) {
        console.log(e);
        res.json(e);
    }
});


router.post('/notification/:id', async (req, res)=>
{   
    console.log(req.body.id);
    try {
        const dltNoti = await notification.deleteOne({ _id: req.body.id });
        const notiData = await notification.find().sort({_id: -1});
        res.json(notiData);
    }
    catch (e) {
        console.log(e);
        res.json(e);
    }
});
module.exports= router;