const router = require('express').Router();
const total = require('../../model/API/FundRequest');

router.get('/', async (req, res)=>{
    try {
        res.render('dashboard/index');
    }catch (e) {
        res.json({message : e});
    }
});

router.get('/getCount', async (req, res) => {
    try {
        const total_count = await total.find({reqStatus: "Pending" }).countDocuments();
        res.json(total_count);
    } catch (error) {
        res.json(error);
    }
});

module.exports = router;