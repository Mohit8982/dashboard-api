const router = require('express').Router();
const provider = require('../../model/games/Games_Provider');
const gameBids = require('../../model/games/gameBids');

router.get('/', async (req, res)=>{
    try {
        const providerList = await provider.find();
        res.render('dashboard/jodiAll', { data : providerList });
    }catch (e) {
        res.json({message : e});
    }
});

router.post('/', async (req, res) => {
    
    const gameSession = req.body.gameSession;
    const gameid = req.body.gameid;
    const sDate = req.body.sDate;
    const eDate = req.body.eDate;

    const bidsData = await gameBids.find({
        gameDate: {
            $gt: sDate,
            $lt: eDate
        },
        gameSession : gameSession, providerId : gameid
    });
    res.json(bidsData);
});


module.exports = router;
