const router = require('express').Router();
const User = require('../../model/API/Users');

router.get('/', async (req, res)=>{
    try {
        const users = await User.find();
        //res.json(users);
        res.render('dashboard/user', { users : users });
    }catch (e) {
        res.json({message : e});
    }
});

router.patch('/:userId', async (req, res)=> {

        try {
                const  updateUser = await  User.updateOne(
                { _id: req.params.userId },
                { $set: { banned : req.body.status }}
            );
            res.redirect('/userList');
        }catch (e) {
            res.redirect('dashboard/user');
        }
});


module.exports = router;
