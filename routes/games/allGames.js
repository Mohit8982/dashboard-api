const router = require('express').Router();
const gamesProvider = require('../../model/games/Games_Provider');
const dateTime = require('node-datetime');

router.get('/', async (req, res)=>{
    try {
        const provider = await gamesProvider.find();
        res.render('./games/provider', { data: provider });
    } catch (e) {
        res.json({ message: e });
    }

});

router.get('/specificUser', async (req, res)=>{
    try {
        const user = await gamesProvider.findOne({ _id: req.query.userId});
        res.json(user);
    } catch (e) {
        res.json({ message: e });
    }
});

router.post('/insertGame', async (req, res)=>{

    const dt = dateTime.create();
    const formatted = dt.format('Y-m-d H:M:S');

    const games = new gamesProvider({
        providerName:  req.body.gamename,
        providerResult: req.body.result,
        modifiedAt: formatted
    });

    try {
        const savedGames = await games.save();
        res.redirect('/games');
    }
    catch (e) {
        res.status(400).send(e);
    }
});

router.delete('/', async (req, res)=>{
    try {
        const savedGames = await gamesProvider.deleteOne({ _id: req.body.userId});
        res.json(savedGames);
    }
    catch (e) {
        res.json(e);
    }
});

router.patch('/', async (req, res)=>{
    try {

        const dt = dateTime.create();
        const formatted = dt.format('Y-m-d H:M:S');

        const  updateUser = await  gamesProvider.updateOne(
            { _id: req.body.userId },
            { $set: { providerName : req.body.gamename, providerResult: req.body.result, modifiedAt: formatted } });
             res.redirect('/games');
    }
    catch (e) {
         res.json(e);
    }
});

module.exports = router;