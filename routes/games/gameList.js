const router = require('express').Router();
const gameList = require('../../model/games/GameList');
const dateTime = require('node-datetime');


router.get('/', async (req, res)=>{

    try {
        const provider = await gameList.find();
        res.render('./games/gameList', { data: provider });
    } catch (e) {
        res.json({ message: e });
    }
});

router.get('/specificUser', async (req, res)=>{
    //console.log(req.query);
    try {
        const user = await gameList.findOne({ _id: req.query.userId});
        res.json(user);
    } catch (e) {
        res.json({ message: e });
    }
});

router.post('/insertGame', async (req, res)=>{

    const dt = dateTime.create();
    const formatted = dt.format('Y-m-d H:M:S');

    const games = new gameList({
        gameName:  req.body.gamename,
        gamePrice: req.body.price,
        modifiedAt: formatted
    });

    try {
        const savedGames = await games.save();
        res.redirect('/gameList');
    }
    catch (e) {
        res.status(400).send(e);
    }
});

router.delete('/', async (req, res)=>{
    try {
        const savedGames = await gameList.deleteOne({ _id: req.body.userId});
        res.json(savedGames);
    }
    catch (e) {
        res.json(e);
    }
});

router.patch('/', async (req, res)=>{
    try {

        const dt = dateTime.create();
        const formatted = dt.format('Y-m-d H:M:S');

        const  updateUser = await  gameList.updateOne(
            { _id: req.body.userId },
            { $set: { gameName : req.body.gamename, gamePrice : req.body.price, modifiedAt: formatted } });
        res.redirect('/gameList');
    }
    catch (e) {
        res.json(e);
    }
});

module.exports = router;
