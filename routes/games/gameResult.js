const router = require('express').Router();
const gamesProvider = require('../../model/games/Games_Provider');
const gameResult = require('../../model/games/GameResult');
const dateTime = require('node-datetime');

router.get('/', async (req, res)=>{
    const dt = dateTime.create();
    const formatted = dt.format('m/d/Y');
    try {
        const name = req.query.name;
        const provider = await gamesProvider.find();
        const result = await  gameResult.find().sort({_id: 1}).where('resultDate').equals(formatted);
        if (name === "mohit"){
            res.json(result);
        }
        else {
            res.render('./games/gameresult' , { data: provider, result: result});
        }
    }
    catch (e) {
        console.log(e);
    }
});

router.delete('/delete', async (req, res)=>{
    try {
        const dltResult = await gameResult.deleteOne({ _id: req.body.userId});
        res.json(dltResult);
    }
    catch (e) {
        res.json(e);
    }
});

router.post('/', async (req, res)=>{
    try {
        const str = req.body.providerId;
        const data = str.split("|");
        const id = data[0];
        const name = data[1];
        const exist = await gameResult.findOne({ providerId: id,resultDate:  req.body.resultDate,session: req.body.session });
        if (!exist) {
            const details = new gameResult({
                providerId: id,
                providerName: name,
                session: req.body.session,
                resultDate: req.body.resultDate,
                winningDigit: req.body.winningDigit,
                status: "1"
            });
            const savedGames = await details.save();
            res.json(1);
        }
        else {
            const data = 'Details Already Filled For : '+ name +', Session : ' + req.body.session + ', Date: ' + req.body.resultDate;
            res.json(data);
        }
    }catch (e) {
        res.json(e);
        console.log(e);
    }
});

router.get('/pastResult', async (req, res)=>{

    try {
        const name = req.query.date;
        const result = await  gameResult.find().where('resultDate').equals(name);
        res.json(result);
    }
    catch (e) {
        console.log(e);
    }
});


module.exports = router;
