const router = require('express').Router();
const gamesProvider = require('../../model/games/Games_Provider');
const gamesSetting = require('../../model/games/AddSetting');
const dateTime = require('node-datetime');
const mongoose = require('mongoose');

router.get('/', async (req, res)=>{
    try {

        const id = req.query.userId;
        gamesProvider.aggregate([
            {
                $lookup: {
                     from: 'games_settings',
                     localField: '_id',
                     foreignField: 'providerId',
                     as: 'gameDetails'
                }
            }
            ],function (error, lookup) {
            if (error) throw error;

            else  {
              if (id == 123456)
              { res.json(lookup); }
              else {res.render('./games/gamesetting', { data: lookup });}
            }

        });
    } catch (e) {
        res.json({ message: e });
    }
});

router.get('/addSetting', async (req, res)=>{
    try {
        const provider = await gamesProvider.find();
        res.render('./games/addSetting', { data: provider });
    } catch (e) {
        res.json({ message: e });
    }
});

router.post('/insertSettings', async (req, res)=>{

    const dt = dateTime.create();
    const formatted = dt.format('Y-m-d H:M:S');

    const settings = new gamesSetting({
        providerId : req.body.gameid,
        gameDay: req.body.gameDay,
        OBT: req.body.game1.toString(),
        CBT: req.body.game2.toString(),
        OBRT: req.body.game3.toString(),
        CBRT: req.body.game4.toString(),
        isClosed: req.body.status,
        modifiedAt: formatted
    });

    try {
        const savedSettings = await settings.save();
        res.redirect('/gamesSetting');
    }
    catch (e) {
        res.status(400).send(e);
    }
});

router.patch('/', async (req, res)=>{
    try {
        const dt = dateTime.create();
        const formatted = dt.format('Y-m-d H:M:S');

        const  updateUser = await  gamesSetting.updateOne(
            { _id: req.body.id },
            { $set: { OBT : req.body.obt, CBT: req.body.cbt, OBRT:  req.body.obrt, CBRT : req.body.cbrt, isClosed: req.body.close, modifiedAt:  formatted, } });
        res.redirect('/gamesSetting');
    }
    catch (e) {
        res.json(e);
    }
});

router.post('/updateAll', async (req, res)=>{
    try {
        const dt = dateTime.create();
        const formatted = dt.format('Y-m-d H:M:S');

        const  updateUser = await  gamesSetting.updateMany(
            { providerId : req.body.providerId },
            { $set: { OBT : req.body.obtTime, CBT: req.body.cbtTime, OBRT:  req.body.obrtTime, CBRT : req.body.cbrtTime, modifiedAt:  formatted, }         });
        res.redirect('/gamesSetting');
    }
    catch (e) {
        res.redirect('./games/multiedit');
    }
});


router.post('/:providerId', async (req, res)=>{

    try {
        const id = mongoose.Types.ObjectId(req.params.providerId);
        gamesProvider.aggregate([
            { $match : { _id : id } },
            {
                $lookup: {
                    from: 'games_settings',
                    localField: '_id',
                    foreignField: 'providerId',
                    as: 'gameDetails',
                }
            }
        ],function (error, result) {
            if (error) throw error;
            else {  res.render('./games/multiedit', { data: result });   }
        });
    } catch (error) {
        res.json({ message: error });
    }
});



module.exports = router;
