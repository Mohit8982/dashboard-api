const router = require('express').Router();
const result = require('../../model/games/GameResult');
const porvider = require('../../model/games/Games_Provider');
const Mongoose = require('mongoose');

router.get('/', async (req, res)=>{
    // const game = Mongoose.Types.ObjectId('5d42d2f992a1d83450eb1571');
    // const exist = await result.findOne({ _id: game});
    // const winningDigit = exist.winningDigit;
    // const digits = winningDigit.toString().split('');
    // const realDigits = digits.map(Number);
    // const sum = realDigits.reduce((a, b) => a + b);
    // const lastDigit = sum % 10;
    // console.log(lastDigit);
    res.render('games/winnerList');

});

module.exports = router;
