const router = require('express').Router();
const Bank = require('../../model/bank');



//banks Route
router.get('/', async (req, res) => {
    try {
        const bank = await Bank.find();
        res.render('./masters/bank', { data: bank });
    } catch (e) {
        res.json({ message: e });
    }
});



router.post('/registerbank', async (req, res) => {      // 0 == Active || 1 == Disabled
    const BankDetails = new Bank({
        bankName: req.body.bankName,
        status: req.body.status,
    });
    try {
        const bank = await BankDetails.save();
        res.json({ "msg": "Success" });
    } catch (e) {
        res.json({ message: e });
    }
});

router.patch('/:userId', async (req, res) => {
    try {
        const bank = await Bank.updateOne(
            { _id: req.params.userId },
            { $set: { status: req.body.status } }
        );
        //res.json(users);
        res.redirect('/masters/');
    } catch (e) {
        res.json({ message: e });
    }
});



module.exports = router;
