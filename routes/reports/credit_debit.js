const router = require('express').Router();
const fundreq = require('../../model/API/FundRequest');

router.get('/', async(req, res)=>{
    try {
        res.render('./reports/creditDebit');
    }
    catch (e) {
        res.json(e);
    }
});

router.post('/report', async(req, res) => {
    
    console.log("hi");
    console.log(req.body);
    const adminId = req.body.adminName;
    const date = req.body.date;
    const reqType = req.body.reqType;
    
    try {
      const reportData = await fundreq.find({ reqDate: date, adminId: adminId, reqType: reqType });
      console.log(reportData);
      res.json(reportData);
    }
    catch (error) {
        console.log(error);
    }
});

module.exports = router;