const router = require('express').Router();
const bank = require('../../model/bank');
const walletHistory = require('../../model/wallet_history');
const userInfo = require('../../model/API/Users');
const fundReport = require('../../model/API/FundRequest');

router.get('/', async(req, res)=>{
    try {
        const bankList = await bank.find();
        res.render('./reports/fundReport',{ data : bankList });
    }
    catch (e) {
        res.json(e);
    }
});


router.post('/', async (req, res) => {
   
    const sdate = req.body.sdate;
    const edate = req.body.edate;
    const bankName = req.body.bankName;
    const reqType = req.body.reqType;
    const admin_id = req.body.admin_id;
    try {
        const data = await walletHistory.find({
            transaction_date: {
                $gt: sdate,
                $lt: edate
            },
            particular: bankName,
            admin_id: admin_id,
            reqType: reqType
        });
        res.json(data);
    }
    catch (error)
    {
        console.log(error);
    }
});

router.get('/dailyReport', async(req, res)=>{
    try {
        res.render('./reports/dailyReport');
    }
    catch (e) {
        res.json(e);
    }
});

router.post('/dailyData', async (req, res) => {
    const type = req.body.reqType;
    const sdate = req.body.sdate;
    const edate = req.body.edate;

    try
    {   
        if (type === "PG") //PG = PLAY GAME
        {
            console.log("hi");
            res.json("hi");
        }
        else if (type === "UR") //UR = USER RESGISTRATION
        { 
            const userData = await userInfo.find({
                CtreatedAt: {
                    $gt: sdate,
                    $lt: edate
                }
            });
            res.json(userData);
        }
        else if (type === "RDP") //RDP = Request For Deposite Point
        {

            const FundData = await fundReport.find({
                reqDate : {
                    $gt: sdate,
                    $lt: edate
                },
                reqType : "Credit"
            });
            res.json(FundData);
        }
        else if (type === "RWP") // RWP = Request For Withdraw Point
        {
            const FundData = await fundReport.find({
                reqDate : {
                    $gt: sdate,
                    $lt: edate
                },
                reqType : "Debit"
            });
            res.json(FundData);
        }
        else if (type === "CRDP") // CRDP = Cancel Request For Deposite Point
        {
            const FundData = await fundReport.find({
                reqDate : {
                    $gt: sdate,
                    $lt: edate
                },
                reqType: "Credit",
                reqStatus: "Declined"
            });
            res.json(FundData);
        }
        else //CRWP = Cancel Request For Withdraw Point
        {
            const FundData = await fundReport.find({
                reqDate : {
                    $gt: sdate,
                    $lt: edate
                },
                reqType: "Debit",
                reqStatus: "Declined"
            });
            res.json(FundData);
        }


    } catch (error)
    {
        console.log(error);
        res.json(error);
    }
});

module.exports = router;