const router = require('express').Router();
const gameBids = require('../../model/games/gameBids');
const starBIds = require('../../model/starline/StarlineBids');

router.get('/usersBidsRatio', async(req, res)=>{
    try {
        res.render('./reports/usersBidsRatio');
    }
    catch (e) {
        res.json(e);
    }
});

router.post('/gameAnalysys', async (req, res) => {
    try {
        gameBids.aggregate([
            { 
                $group : { 
                    _id: "$userId",
                    countBids        : { $sum: 1 },
                    sumbiddingPoints :    { $sum: '$biddingPoints' },
                    sumWinPoint: { $sum: '$gameWinPoints' },
                    username: { "$first": "$userName" },
                    pf: {
                        $first: {
                            $cond: {
                            if: { $gt : [{ $sum: '$biddingPoints' }, { $sum: '$gameWinPoints' }] }, then: "In Loss", else: "In Profit"
                        }
                    }},
                    winCount: {
                        "$sum": {
                            $cond: {
                                if: { $eq: ['$winStatus', 'Win'] }, then: 1, else: 0
                            }
                        }
                    },               
                    loseCount: {
                        "$sum": {
                            $cond: {
                                if: { $eq: ['$winStatus', 'Loss'] }, then: 1, else: 0
                            }
                        }
                    },
                    pendingCount: {
                        "$sum": {
                            $cond: {
                                if: { $eq: ['$winStatus', 'Pending'] }, then: 1, else: 0
                            }
                        }
                    }
                }
            }
        ], function (error, group) {
                if (error) throw error;
                else res.json(group);
        });
    } catch (error) {
        console.log(error);
        res.json(error);
    }
});

router.get('/userStarlineRatio', async(req, res)=>{
    try {
        res.render('./reports/userStarlineRatio');
    }
    catch (e) {
        res.json(e);
    }
});

router.post('/gameAnalysysStar', async (req, res) => {
    try {
        starBIds.aggregate([
            { 
                $group : { 
                    _id: "$userId",
                    countBids        : { $sum: 1 },
                    sumbiddingPoints :    { $sum: '$biddingPoints' },
                    sumWinPoint: { $sum: '$gameWinPoints' },
                    username: { "$first": "$userName" },
                    pf: {
                        $first: {
                            $cond: {
                            if: { $gt : [{ $sum: '$biddingPoints' }, { $sum: '$gameWinPoints' }] }, then: "In Loss", else: "In Profit"
                        }
                    }},
                    winCount: {
                        "$sum": {
                            $cond: {
                                if: { $eq: ['$winStatus', 'Win'] }, then: 1, else: 0
                            }
                        }
                    },               
                    loseCount: {
                        "$sum": {
                            $cond: {
                                if: { $eq: ['$winStatus', 'Loss'] }, then: 1, else: 0
                            }
                        }
                    },
                    pendingCount: {
                        "$sum": {
                            $cond: {
                                if: { $eq: ['$winStatus', 'Pending'] }, then: 1, else: 0
                            }
                        }
                    }
                }
            }
        ], function (error, group) {
                if (error) throw error;
                else res.json(group);
        });
    } catch (error) {
        console.log(error);
        res.json(error);
    }
});


module.exports = router;