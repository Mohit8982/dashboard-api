const router = require('express').Router();
const user = require('../../model/API/Users');
const provider = require('../../model/games/Games_Provider');
const starProvider = require('../../model/starline/Starline_Provider');
const abProvider = require('../../model/AndarBahar/ABProvider');
const bids = require('../../model/games/gameBids');
const starBids = require('../../model/starline/StarlineBids');
const abBids = require('../../model/AndarBahar/ABbids');

router.get('/', async(req, res)=>{
    try {
        const providerData = await provider.find();
        res.render('./reports/salesReport', { data: providerData});
    }
    catch (e) {
        res.json(e);
    }
});

router.get('/starline', async(req, res)=>{
    try {
        const providerData = await starProvider.find();
        res.render('./reports/starlineSalesReport', { data: providerData});
    }
    catch (e) {
        res.json(e);
    }
});

router.get('/andarBahar', async(req, res)=>{
    try {
        const providerData = await abProvider.find();
        res.render('./reports/andarBaharReport', { data: providerData});
    }
    catch (e) {
        res.json(e);
    }
});

router.get('/getUsername', async (req, res) => {
    try {
        const userData = await user.find();
        res.json(userData);
    } catch (error) {
        res.json(error);
    }   
});

router.post('/userReport', async (req, res) => {
    try {
        const bidsData = await bids.find({
            gameDate: {
                $gt: req.body.startDate,
                $lt: req.body.endDate
            },
            userId : req.body.userId, providerId : req.body.gameId
        });
        res.json(bidsData);
    } catch (error) {
        console.log(error);
    }
});

router.post('/userReportStar', async (req, res) => {
    try {
        const bidsData = await starBids.find({
            gameDate: {
                $gt: req.body.startDate,
                $lt: req.body.endDate
            },
            userId : req.body.userId, providerId : req.body.gameId
        });
        res.json(bidsData);
    } catch (error) {
        console.log(error);
    }
});

router.post('/userReportAB', async (req, res) => {
    try {
        const bidsData = await abBids.find({
            gameDate: {
                $gt: req.body.startDate,
                $lt: req.body.endDate
            },
            userId : req.body.userId, providerId : req.body.gameId
        });
        res.json(bidsData);
    } catch (error) {
        console.log(error);
    }
});

module.exports = router;