const router = require('express').Router();
const abProvider = require('../../model/AndarBahar/ABProvider');
const abGame = require('../../model/AndarBahar/ABGameList');
const abBids = require('../../model/AndarBahar/ABbids');
const gameBids = require('../../model/games/gameBids');
const gamesProvider = require('../../model/games/Games_Provider');
const gamesList = require('../../model/games/GameList');
const starBids = require('../../model/starline/StarlineBids');
var mongodb = require('mongodb');

router.get('/andarBaharBids', async(req, res)=>{
    try {
        const providerData = await abProvider.find();
        const gameData = await abGame.find();
        res.render('./reports/andarBaharBids', { data: providerData, data1 : gameData});
    }
    catch (e) {
        res.json(e);
    }
});

router.post('/andarBaharBidsData', async (req, res) => {
    try {
        const bidsData = await abBids.find({
            gameDate: req.body.startDate, gameTypeId : req.body.gameTypeid, providerId : req.body.gameId
        });
        res.json(bidsData);
    } catch (error) {
        console.log(error);
    }
});

router.get('/games', async (req, res)=>{
    try {
        const array = [
            gamesProvider.find().exec(),
            gamesList.find().exec(),
        ];
        Promise.all(array).then(([provider, list]) => {
            res.render('./reports/totalBids', { provider: provider, list: list });
        })
    } catch (e) {
        res.json({ message: e });
    }
});

router.get('/gameBidsData', async (req, res) => {
    const providerName = req.query.providerName;
    const gameType = req.query.gameType;
    const session = req.query.session;
    const date = req.query.date;
    try {
        const bidsData = await gameBids.find({ providerId: providerName, gameTypeId: gameType, gameSession: session, gameDate: date });
        res.json(bidsData); 
    } catch (error) {
        console.log(error);
    }
});


router.get('/biddingReport', async (req, res) => {
    try {
        const array = [
            gamesProvider.find().exec(),
            gamesList.find().exec()
        ];
        Promise.all(array).then(([provider, list]) => {
            res.render('./reports/biddingReport', { provider: provider, list: list });
        })
    } catch (error) {
        console.log(error);
    }
});

router.post('/biddingDay', async (req, res) => {
    const provider = req.body.provider;
    const date = req.body.date;
    const gameType = req.body.gameType;
    const session = req.body.session;
    try
    {       
        const data = await gameBids.aggregate([
            {
                $match: {
                    providerId: mongodb.ObjectId(provider),
                    gameTypeId :mongodb.ObjectId(gameType),
                    gameDate: date,
                    gameSession: session
                }
            },
            { 
                $group : { 
                    _id: "$bidDigit",
                    sumdigit: { $sum: '$biddingPoints' },
                    "gameDate": { "$first": "$gameDate" },
                    "winningDigit": { "$first": "$gameWinPoints" },
                    "bidDigit": { "$first": "$bidDigit" }
                }
            }
       ], function (error, group) {
                if (error) throw error;
                else res.json(group);
        });
    } catch (error) {
        console.log(error);
    }
});

router.get('/allUserBids', async (req, res) => {
    try {
        res.render('./reports/allUserBids');
    } catch (error) {
        res.json(error);
    }
});


router.post('/getUserBidData', async (req, res) => {
    try {    
        const market = req.body.marketType;
        const userId = req.body.userId;
        let marketType = gameBids;
        let alldata;
        if (market == 2)
        {
            marketType = starBids;
            alldata  = await starBids.find({ userId: mongodb.ObjectId(userId) })
        }
        else {
            alldata  = await gameBids.find({ userId: mongodb.ObjectId(userId) })
        }

        const groupData = await marketType.aggregate([
            {
                $match: {
                    userId: mongodb.ObjectId(userId)
                }
            },
            { 
                $group : { 
                    _id: "$providerId",
                    sumdigit: { $sum: '$biddingPoints' },
                    countBid: { $sum : 1 },
                    "providerName": { "$first": "$providerName" },
                    "gameTypeName": { "$first": "$gameTypeName" }
                }
            }
       ]);

        res.json({group: groupData, data :  alldata});

   } catch (error) {
        console.log(error);
   }
});


//pagination
// router.get('/:page', async (req, res) => {
//     var perPage = 50;
//     var page = req.params.page || 1;
//     try {
//         gameBids.find({})
//         .skip((perPage * page) - perPage)
//         .limit(perPage).exec(function(err,data){
//          if(err) throw err;
//             gameBids.countDocuments({}).exec((err, count) => {     
//         res.render('./reports/allBids', { records: data,current: page,pages: Math.ceil(count / perPage) });
//         });
//     });
//     } catch (error) {
//         console.log(error);
//     }
// });

module.exports = router;