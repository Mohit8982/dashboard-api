const router = require('express').Router();
const starProvider = require('../../model/starline/Starline_Provider');
const starSettings = require('../../model/starline/AddSetting');
const dateTime = require('node-datetime');
const mongoose = require('mongoose');

router.get('/', async (req, res)=>{

    try {
        const id = req.query.userId;
        starProvider.aggregate([
            {
                $lookup: {
                    from: 'starline_games_settings',
                    localField: '_id',
                    foreignField: 'providerId',
                    as: 'gameDetails'
                }
            }
        ],function (error, lookup) {
            if (error) throw error;
            else{
                if (id == 123456)
                { res.json(lookup); }
                else {res.render('./starline/starlinegamesetting', { data: lookup });}
            }
        });
    } catch (e) {
        res.json({ message: e });
    }
});

router.get('/addSetting', async (req, res)=>{
    try {
        const provider = await starProvider.find();
        res.render('./starline/starlineaddsetting', { data: provider });
    } catch (e) {
        res.json({ message: e });
    }
});


router.post('/updateProviderSettings', async (req, res)=>{
    try {
        const dt = dateTime.create();
        const formatted = dt.format('Y-m-d H:M:S');

        const  updateUser = await  starSettings.updateMany(
            { providerId : req.body.providerId },
            { $set: { OBT : req.body.obtTime, CBT: req.body.cbtTime, OBRT:  req.body.obrtTime, modifiedAt:  formatted, }         });
        res.redirect('/starlinegamesetting');
    }
    catch (e) {
        res.redirect('./starline/starlinemultiedit');
    }
});

router.patch('/', async (req, res)=>{
    try {
        const dt = dateTime.create();
        const formatted = dt.format('Y-m-d H:M:S');
        const  updateUser = await  starSettings.updateOne(
            { _id: req.body.id },
            { $set: { OBT : req.body.obt, CBT: req.body.cbt, OBRT:  req.body.obrt, isClosed: req.body.close, modifiedAt:  formatted, } });
        res.redirect('/starlinegamesetting');
    }
    catch (e) {
        res.json(e);
    }
});

router.post('/insertSettings', async (req, res)=>{

    const dt = dateTime.create();
    const formatted = dt.format('Y-m-d H:M:S');
     const settings = new starSettings({
        providerId : req.body.gameid,
        gameDay: req.body.gameDay,
        OBT: req.body.game1,
        CBT: req.body.game2,
        OBRT: req.body.game3,
        isClosed: req.body.status,
        modifiedAt: formatted
    });

    try {
        const savedSettings = await settings.save();
        res.redirect('/starlinegamesetting');
    }
    catch (e) {
        res.status(400).send(e);
    }
});

router.post('/:providerId', async (req, res)=>{
    try {
        const id = req.params.providerId;
        const findMultiple = await starSettings.find({providerId : id});
        if (Object.keys(findMultiple).length === 0){
            res.render('./starline/starlinemultiedit', { data: "EMPTY" });
        }
        else {
            res.render('./starline/starlinemultiedit', { data: findMultiple });
        }

    } catch (error) {
        console.log(error);
        res.json({ message: error });
    }
});



module.exports = router;
