const router = require('express').Router();
const starlineProvider = require('../../model/starline/Starline_Provider');
const dateTime = require('node-datetime');

router.get('/', async (req, res)=>{

    try {
        const provider = await starlineProvider.find();
        res.render('./starline/starLineProvider', { data: provider });
    } catch (e) {
        res.json({ message: e });
    }

});

router.get('/specificUser', async (req, res)=>{
    //console.log(req.query);
    try {
        const user = await starlineProvider.findOne({ _id: req.query.userId});
        res.json(user);
    } catch (e) {
        res.json({ message: e });
    }
});

router.post('/insertGame', async (req, res)=>{

    const dt = dateTime.create();
    const formatted = dt.format('Y-m-d H:M:S');

    const games = new starlineProvider({
        providerName:  req.body.gamename,
        providerResult: req.body.result,
        modifiedAt: formatted
    });

    try {
        const savedGames = await games.save();
        res.redirect('/starlineProvider');
    }
    catch (e) {
        res.status(400).send(e);
    }
});

router.delete('/', async (req, res)=>{
    try {
        const savedGames = await starlineProvider.deleteOne({ _id: req.body.userId});
        res.json(savedGames);
    }
    catch (e) {
        res.json(e);
    }
});

router.patch('/', async (req, res)=>{
    try {

        const dt = dateTime.create();
        const formatted = dt.format('Y-m-d H:M:S');

        const  updateUser = await  starlineProvider.updateOne(
            { _id: req.body.userId },
            { $set: { providerName : req.body.gamename, providerResult: req.body.result, modifiedAt: formatted } });
             res.redirect('/starlineProvider');
    }
    catch (e) {
         res.json(e);
    }
});
module.exports = router;
