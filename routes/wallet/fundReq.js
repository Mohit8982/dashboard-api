const router = require('express').Router();
const User = require('../../model/API/Users');
const fundReq = require('../../model/API/FundRequest');
const Userprofile = require('../../model/API/Profile');
const wallet_hstry = require('../../model/wallet_history');
const dateTime = require('node-datetime');
const dt = dateTime.create();
const formatted = dt.format('m/d/y');
router.get('/', async (req, res)=>{
    const pendingCredit = await fundReq.find({reqStatus : 'Pending',reqType: 'Credit'}).sort({_id: -1}).limit(200);
    res.render('./wallet/fundRequest', {data : pendingCredit});
});

router.get('/other', async (req, res)=>{
    const data = req.query.data;

    if (data=="pendingCredit") {
        const pendingCredit = await fundReq.find({ reqStatus: 'Pending', reqType: 'Credit' }).sort({ _id: -1 }).limit(200);
        res.json(pendingCredit);
    }
    else if (data=="approvedReq")
    {
        const approvedCredit = await fundReq.find({reqStatus : 'Approved',reqType: 'Credit' }).sort({_id: -1}).limit(200);
        res.json(approvedCredit);
    }
    else if(data == "pendingDebit"){
        const pendingDebit = await fundReq.find({reqStatus : 'Pending',reqType: 'Debit'}).sort({_id: -1}).limit(200);
        res.json(pendingDebit);
    }
    else if(data == "approvedDebit"){
        const approvedDebit = await fundReq.find({reqStatus : 'Approved',reqType: 'Debit'}).sort({_id: -1}).limit(200);
        res.json(approvedDebit);

    }
    else if(data == "declinedDebit"){
        const declinedDebit = await fundReq.find({reqStatus : 'Declined',reqType: 'Debit'}).sort({_id: -1}).limit(200);
        res.json(declinedDebit);
    }
    else if(data == "completed"){
        const completed = await fundReq.find({reqStatus : 'Approved'}).sort({_id: -1}).limit(100);
        res.json(completed);
    }
});


router.patch('/updateWallet/:id', async (req, res)=>{
    let id = req.params.id;
    const rowId = req.body.rowId;
    const bal = req.body.amount;
    const userId = req.body.userId;
    const username = req.body.username;
    const adminId = '5d7ca8b1c9dd421df85d05ac';

    try {
        const user = await User.findOne({ _id: userId});
        const wallet_bal = user.wallet_balance;
        let update_bal = '';
        let detail = '';
        let particular = '';
        let reqType;
        if (id == 1)
        {
            update_bal = wallet_bal + parseInt(bal);
            detail = 'Amount Added To Wallet By Admin';
            particular = 'Crdeit Request';
            reqType = 'Credit'
        }
        else{
            update_bal = wallet_bal - parseInt(bal);
            detail = 'Amount Withdrawn From Wallet By Admin';
            particular = 'SBI Bank';
            reqType = 'Debit';
        }

        const  updateUser = await User.updateOne(
            { _id: userId },
            { $set: { wallet_balance : update_bal, wallet_bal_updated_at : formatted }});   

        const updateStatus = await fundReq.updateOne(
            { _id : rowId },
            { $set: { reqStatus: 'Approved', reqUpdatedAt: formatted, UpdatedBy: "Admin1", adminId: adminId } }); //Admin1 & AdminId To be Changed TO LOGIN USERNAME and ID.
                   
        if (updateStatus.nModified === 1)
        {
            const history = new  wallet_hstry({
                userId  : userId,
                previous_amount: wallet_bal,
                current_amount: update_bal,
                transaction_amount: parseInt(bal),
                description: detail,
                transaction_date: formatted,
                transaction_status: 'Success',
                admin_id: '1324546', // to be changed to admin id
                addedBy_name: 'Admin', //To Be Changed after session work done
                particular: particular,   
                reqType: reqType,
                username: username
            });
            const addHistory = await history.save();
            res.json(updateStatus);
        }
        else {
            res.json("Sorry Cannot Modify At Present");
        }
    
    } catch (e) {
        console.log(e);
        res.json({ message: e });
    }
});


router.patch('/decline', async (req, res)=>{
    const rowId = req.body.rowId;
    const adminId = '5d7ca8b1c9dd421df85d05ac';
    try {
        console.log(rowId);
        const updateDecline = await  fundReq.updateOne(
            {_id : rowId },
            { $set : { reqStatus : 'Declined', reqUpdatedAt : formatted, UpdatedBy : "Admin1",adminId: adminId }});
        console.log(updateDecline);
        res.json(updateDecline);
    } catch (e) {
        console.log(e);
        res.json({ message: e });
    }
});

router.get('/getProfile', async (req, res)=>{

    const id = req.query.userId;
    const user = await User.findOne({ _id: id});
    const userprofile = await Userprofile.findOne({userId : id});
    try {
        const userData = {
            userData1 : user,
            userData2 : userprofile
        };
        res.json(userData);
    }
    catch (e) {
        console.log(e);
        res.json(e);
    }

});

module.exports = router;
