const router = require('express').Router();

router.get('/', async(req, res)=>{
    try {
        res.render('./wallet/reqOnOff');
    }
    catch (e) {
        console.log(e);
        res.json(e);
    }
});

module.exports = router;
