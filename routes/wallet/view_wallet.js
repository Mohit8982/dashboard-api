const router = require('express').Router();
const User = require('../../model/API/Users');
const wallet_history = require('../../model/wallet_history');
const bank = require('../../model/bank');
const dateTime = require('node-datetime');
const dt = dateTime.create();
const formatted = dt.format('Y-m-d H:M:S');

router.get('/', async (req, res)=>{
    const banklist = await bank.find({status : 0});
    res.render('./wallet/view_Wallet', {data : banklist});
});

router.get('/getData', async (req, res)=>{
    try {
        const userData = await User.find();
        res.json(userData);
    }
    catch (e) {
        res.json(e);
    }
});

router.post('/updateData', async (req, res)=>{
    // 1 : credit,
    // 0 : debit
    const id = req.body.id;
    const bal = req.body.amount;
    const type = req.body.type;
    const particularName = req.body.particular;

    try {
        const user = await User.findOne({ _id: id});
        const wallet_bal = user.wallet_balance;
        let update_bal = '';
        let detail = '';
        let particular = '';
        let reqType = '';

        if (type == 1)
        {
            update_bal = wallet_bal + parseInt(bal);
            detail = "Amount Added To Wallet By Admin";
            particular = particularName;
            reqType = "Credit";
        }
        else{
            update_bal = wallet_bal - parseInt(bal);
            detail = "Amount Withdrawn From Wallet By Admin";
            particular = particularName;
            reqType = "Debit";
        }
        const  updateUser = await  User.updateOne(
            { _id: id },
            { $set: { wallet_balance : update_bal, wallet_bal_updated_at : formatted }});

        const history = new  wallet_history({
            userId  : id,
            previous_amount: wallet_bal,
            current_amount: update_bal,
            transaction_amount: parseInt(bal),
            description: detail,
            transaction_date: formatted,
            transaction_status: "Success",
            admin_id: "1324546",
            particular: particular,
            username: "Mohit 8982",
            reqType: reqType,
            addedBy_name: "Admin 1" //To be Changed after login
        });
        const addHistory = await history.save();
        res.json(addHistory);

    } catch (e) {
        console.log(e);
        res.json({ message: e });
    }

});

router.get('/history', async (req, res)=>{

    const id = req.query.id;
    try {
        const hisData = await wallet_history.find({userId : id});
        res.json(hisData);
    }
    catch (e) {
        console.log(e);
        res.json(e);
    }
});

module.exports = router;
