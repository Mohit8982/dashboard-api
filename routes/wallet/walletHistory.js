const router = require('express').Router();
const w_history = require('../../model/wallet_history');

router.get('/history', async(req, res)=>{
    try {
        const historyData = await w_history.find();
        res.json(historyData);
    }
    catch (e) {
        console.log(e);
        res.json(e);
    }
});
