//validation
const Joi = require('@hapi/joi');


const registerValidation = data =>{

    const schema =
        {
            name: Joi.string().min(3).required(),
            password: Joi.string().min(5).required(),
            email: Joi.string().min(6).required().email(),
            username: Joi.string().min(5).required(),
            verified: Joi.string().required(),
            role: Joi.string().required(),
            mobile: Joi.string().required(),
            firebaseId: Joi.string().required(),
            token: Joi.string().required(),
            deviceName: Joi.string().required(),
            deviceId: Joi.string().required(),
            banned: Joi.string().required(),
            deviceVeriOTP: Joi.string().required(),
            wallet_balance: Joi.number().integer().required(),
            wallet_bal_updated_at: Joi.string().required(),
            mpin: Joi.string().required(),
            mpinOtp: Joi.string().required()
        };

    return Joi.validate(data, schema);
};

const loginValidation = data =>{
    const schema =
        {
            username: Joi.string().min(5).required(),
            password: Joi.string().min(5).required()
        };
    return Joi.validate(data, schema);
};

const bidsValidation = data =>{
    const schema = {
        username: Joi.string().required(),
        userId: Joi.string().required(),
        game_provider_id: Joi.string().required(),
        game_provider_name: Joi.string().required(),
        game_type_name: Joi.string().required(),
        game_type_id: Joi.string().required(),
        game_bid_no: Joi.number().required(),
        game_bidding_points: Joi.number().required(),
        win_status: Joi.string().allow(''),
        game_winning_points: Joi.string().allow(''),
        game_date:  Joi.string().required(),
        game_session:  Joi.string().required(),
        created_at:  Joi.string().required(),
        updatedAt:  Joi.string().allow('')
    };
    return Joi.validate(data, schema);
};

module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
module.exports.bidsValidation = bidsValidation;
